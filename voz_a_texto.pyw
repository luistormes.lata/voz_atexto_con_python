import speech_recognition as sr
import pyaudio
r = sr.Recognizer()
microfono = sr.Microphone(device_index=12)
with microfono as source:
	r.adjust_for_ambient_noise(source)
	audio = r.listen(source)
print(r.recognize_google(audio ,language='es-ES'))

